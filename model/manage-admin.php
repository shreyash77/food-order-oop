<?php
include "../connection.php";

class AdminData extends Connection
{
    public $conn;

    // public function __construct()
    // {
    //     $this->conn = new Connection;
    // }
    public function getAdminData()
    {
        $connect = $this->connect();
        $sql = "SELECT * FROM tbl_admin;";
        $res = mysqli_query($connect, $sql);
        return $res;
    }
}