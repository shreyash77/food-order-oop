<?php include('./partials/menu.php')?>
<div class="main-content">
    <div class="wrapper">
        <h1>Update Food</h1>
        <br><br>
        <?php
        include "../controller/update-food-controller.php";
            $id = $_GET["id"];
            $getFoodData = new UpdateFoodController();
            $row = $getFoodData->getFooddata($id);
        ?>

        <form action="../controller/update-food-controller.php" method="POST">
            <table class="tbl-30">
                <tr>
                    <td>
                        Title
                    </td>
                    <td>
                        <input type="text" name="title" value="<?php echo $row["title"] ?>">
                    </td>
                </tr>
                <tr>
                    <td>
                        Description
                    </td>
                    <td>
                        <input type="text" name="description" value="<?php echo $row["description"] ?>">
                    </td>
                </tr>
                <tr>
                    <td>
                        Price
                    </td>
                    <td>
                        <input type="text" name="price" value="<?php echo $row["price"] ?>">
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <input type="hidden" name="id" value="<?php echo $id ?>">
                        <input type="submit" name="update" value="update Food" class="btn-secondary">
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>
<?php include('./partials/footer.php')?>