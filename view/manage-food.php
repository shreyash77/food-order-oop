<?php
    include "./partials/menu.php";
    include "../controller/manage-food-controller.php";
    include "../helper/session-helper.php";
?>
<div class="main-content">
    <div class="wrapper">
        <h1>Managa Food</h1>
        <br>
        <strong><?php echo flash("delete admin") ?></strong>
        <br>
        <a href="add-food.php" class="btn-primary text-deco">Add Food</a>
        <br><br>
        <table class="tbl-full">
            <tr>
                <th>S.N</th>
                <th>Title</th>
                <th>Price</th>
                <th>description</th>
                <th>Action</th>
            </tr>
            <?php
                $displayFood = new DisplayFood();
                $foodData = $displayFood->sendFood();
                $sn=1;
                // print_r($foodData);
                while ($rows = mysqli_fetch_assoc($foodData)) {
                    $id = $rows["id"];
                    $title = $rows["title"];
                    $price = $rows["price"];
                    $description = $rows["description"];
                ?>
                    <tr>
                        <td>
                            <?php echo $sn++ ?>
                        </td>
                        <td>
                            <?php echo $title ?>
                        </td>
                        <td>
                            <?php echo $price ?>
                        </td>
                        <td>
                            <?php echo $description ?>
                        </td>
                        <td>
                            <a href="http://localhost/food-site/food-order-oop/view/order-food.php?id=<?php echo $id ?>" class="btn-primary text-deco">Order Food</a>
                            <a href="http://localhost/food-site/food-order-oop/view/update-food.php?id=<?php echo $id ?>" class='btn-secondary text-deco'>Update Food</a>
                            <a href="http://localhost/food-site/food-order-oop/controller/delete-food-controller.php?id=<?php echo $id ?>" class='btn-danger text-deco'>Delete Food</a>
                        </td>
                    </tr>
            <?php
                }
            ?>
        </table>
    </div>
</div>

<?php include "./partials/footer.php"; ?>