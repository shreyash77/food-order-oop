<?php

class Connection
{
    protected function connect()
    {
        try {
            $servername = "localhost";
            $username = "root";
            $password = "";
            $dbn = "food-order";

            $conn = mysqli_connect($servername, $username, $password, $dbn);
            return $conn;

        } catch (Exception $err) {
            die("could not connect to DB" . $err);
        }
    }
}






