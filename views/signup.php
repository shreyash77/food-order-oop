<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link rel="stylesheet" href="../css/admin.css">
</head>
<body>
    <div class="signup">
        <h1 class="text-center">SignUp</h1>
        <br><br>
        <div>
            <?php //giveLoginStatus() ?>
        </div>
        <form action="" method="POST">
            Full Name: <br>
            <input type="text" name="full_name" placeholder="Enter Full Name"><br>
            Username: <br>
            <input type="text" name="username" placeholder="Enter Username"><br>
            Email: <br>
            <input type="text" name="email" placeholder="Enter email"><br>
            Password: <br>
            <input type="password" name="password" placeholder="Enter password"><br>
            Confirm Password: <br>
            <input type="password" name="confirm_password" placeholder="Rewrite password"><br><br>
            <input type="submit" name="submit" value="Login" class="btn-primary">
            <br><br>
        </form>
        <p class="text-center"> Created By: Shreyash Shrestha</p>
    </div>
</body>
</html>