<?php include('./partials/menu.php')?>

<div class="main-content">
    <div class="wrapper">
        <h1>Add Food</h1>
        <br>
        <form action="../app/Controller/AddFoodController.php" method="POST" enctype="multipart/form-data">
            <?php
                include "../helper/session-helper.php";
                flash("add food");
            ?>
            <table class="tbl-30">
                <tr>
                    <td>
                        Title
                    </td>
                    <td>
                        <input type="text" name="title" placeholder="Enter Title of food">
                    </td>
                </tr>
                <tr>
                    <td>
                        description
                    </td>
                    <td>
                        <input type="text" name="description" placeholder="Enter description of food">
                    </td>
                </tr>
                <tr>
                    <td>
                        Price
                    </td>
                    <td>
                        <input type="text" name="price" placeholder="Enter price of food">
                    </td>
                </tr>
                <tr>
                    <td>
                        Image of food
                    </td>
                    <td>
                        <input type="file" name="food_image">
                    </td>
                </tr>
                <tr>
                    <td>
                        <input type="submit" name="submit" class="btn-secondary">
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>

<?php include 'partials/footer.php'; ?>