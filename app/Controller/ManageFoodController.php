<?php
include "../Model/ManageFood.php";

class ManageFoodController
{
    public $foodData;
    function __construct()
    {
        $this->foodData = new ManageFood;
    }

    public function sendFood(): object
    {
        $DbRes = $this->foodData->getfoodData();
        return $DbRes;
    }
}
