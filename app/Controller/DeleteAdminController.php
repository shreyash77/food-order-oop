<?php

include "../Model/DeleteAdmin.php";
include "../helper/session-helper.php";

class DeleteAdminController
{
    public $deleteAdmin;

    public function __construct()
    {
        $this->deleteAdmin = new DeleteAdmin();
    }

    public function checkAdminDelete()
    {
        $deleteRes = $this->deleteAdmin->getAdminAndDelete();

        if ($deleteRes == true) {
            header("location: http://localhost/food-site/food-order-oop/view/manage-admin.php");
            flash("delete admin", "Admin Deleted Successfully");
        }
    }
}

$deleteAdmin = new DeleteAdminController();
$deleteAdmin->checkAdminDelete();