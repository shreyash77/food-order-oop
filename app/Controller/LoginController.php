<?php
include "../Model/Login.php";
include "../helper/session-helper.php";

class LoginController
{
    public $connLogin;
    public $username;
    private $password;

    public function __construct()
    {
        $this->connLogin = new Login;
    }

    public function loginAction(): void
    {
        if (isset($_POST["submit"])) {
            $this->username = $_POST["user_name"];
            $this->password = $_POST["password"];
            if (empty($this->username) || empty($this->password)) {
                flash("login", "Please fill out all inputs");
                header("location:http://localhost/food-site/food-order-oop/view/login.php");
            } else {
                $this->connLogin->checkAdminUsernamePassword($this->username, $this->password);
            }
        }
    }
}

    $login = new LoginController();
    $login->loginAction();
?>