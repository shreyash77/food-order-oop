<?php
include "../connection.php";

class ManageFood extends Connection
{
    public function getFoodData(): object
    {
        $connect = $this->connect();
        $sql = "SELECT * FROM tbl_food;";
        $res = mysqli_query($connect, $sql);
        return $res;
    }
}