<?php

if (!isset($_SESSION)) {
    session_start();
}

if (!function_exists('flash')) {
    function flash($name = '', $message = ''): void
    {
        if (!empty($name)) {
            if (!empty($message) && empty($_SESSION["name"])) {
                $_SESSION["name"] = $message;
            } elseif (empty($message) && !empty($_SESSION["name"])) {
                echo "<div style='color:grey;'>" . $_SESSION["name"] . "</div>";
                unset($_SESSION["name"]);
            }
        }
    }
}
