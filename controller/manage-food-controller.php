<?php
include "../model/manage-food.php";

class DisplayFood
{
    public $foodData;
    function __construct()
    {
        $this->foodData = new FoodData;
    }
    public function sendFood(): object
    {
        $DbRes = $this->foodData->getfoodData();
        return $DbRes;
    }
}
