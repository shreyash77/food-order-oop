<?php

include "../model/add-food.php";
include "../helper/session-helper.php";
include "validation-controller.php";

class AddFoodController extends ValidationCheck
{
    public $title;
    public $description;
    public $price;
    public $addFood;
    public $validationCheck;
    public $imgName;
    public $imgSize;
    public $temName;

    public function __construct()
    {
        $this->addFood = new AddFoodToDB();
        $this->validationCheck = new ValidationCheck();
    }

    public function addFood(): void
    {
        if (isset($_POST["submit"])) {
            $this->title = $_POST["title"];
            $this->description = $_POST["description"];
            $this->price = $_POST["price"];
            $this->imgName = $_FILES["food_image"]["name"];
            $this->imgSize = $_FILES["food_image"]["size"];
            $this->temName = $_FILES["food_image"]["tmp_name"];

            if (!$this->emptyInputForUpdateFood($this->title, $this->description, $this->price)) {
                flash("add-food", "Please fill the input");
                header("location: ../view/add-food.php");
            } elseif (!$this->checkFoodTaken($this->title)) {
                flash("add-food", "Please use different title");
                header("location: ../view/add-food.php");
            } else {
                if ($this->imgSize > 905000) {
                    flash("add food", "file size excceed");
                    header("location: ../view/add-food.php");
                } else {
                    $imgExt = pathinfo($this->imgName, PATHINFO_EXTENSION);
                    $imgExtLower = strtolower($imgExt);
                    $allowedImgExt = ["jpg", "jpeg", "png"];

                    if (in_array($imgExtLower, $allowedImgExt)) {
                        $newImgName = uniqid("img-", true).".".$imgExtLower;
                        $imgUploadPath = "../upload/" . $newImgName;
                        move_uploaded_file($this->temName, $imgUploadPath);
                        $this->addFood->setFood($this->title, $this->description, $this->price, $newImgName);
                        flash("add-food", "added successfully");
                        header("location: ../view/manage-food.php");
                    } else {
                        flash("add food", "you cant upload this type of file");
                        header("location: ../view/add-food.php");
                    }
                }
            }
        }
    }
}

(new AddFoodController())->addFood();
